package at.ac.fhcampuswien.shapes;

public class Rectangle {

    // Klassenvariablen

    private int x1;
    private int x2;
    private int y1;
    private int y2;
    private static int count;

    // Konstruktor all vier Integer Werte

    public Rectangle(int punktx1, int punkty1, int punktx2, int punkty2) {

      this.x1 = punktx1;
      this.x2 = punktx2;
      this.y1 = punkty1;
      this.y2 = punkty2;
      count++;


    }

    // Konstruktor für Höhe und Breite eines Rechteckes

    public Rectangle(int punktx2, int punkty2){

        this.x1 = 0;
        this.x2 = punktx2;
        this.y1 = 0;
        this.y2 = punkty2;
        count++;

    }

    // Konstruktor mit leerer Parameterliste

    public Rectangle () {

        this(0,0,0,0);

    }

    //* Objektmethoden

    // Liefert die Breite des Rechtecks zurück.


    public int getWidth () {

        return Math.abs(x1 - x2);

    }

    // Liefert die Höhe des Rechtecks zurück.

    public int getHeight () {

        return Math.abs(y1 - y2);

    }

    // Bewegt das Rechteck um die entsprechenden Differenzen auf der x und y Achse.

    public void move (int deltax, int deltay) {

        x1 += deltax;
        x2 += deltax;
        y1 += deltay;
        y2 += deltay;

    }

    // Überprüft ob ein Punkt (x,y) innerhalb des Rechtecks liegt und liefert entsprechend true|false zurück.


    public boolean isInside (int x, int y) {

        if(x >= Math.min(x1, x2) && x <= Math.max(x1, x2) && y >= Math.min(y1, y2) && y <= Math.max(y1, y2))
            return true;
        else
            return false;
    }

    // Klassenmethoden


    public static int getCount() {

        return count;

    }

    public int getX1() {
        return x1;
    }

    public int getX2() {
        return x2;
    }

    public int getY1() {
        return y1;
    }

    public int getY2() {
        return y2;
    }

    public static Rectangle union(Rectangle r1, Rectangle r2){
        int minX = Math.min(r1.getX1(), Math.min(r1.getX2(), Math.min(r2.getX1(), r2.getX2())));
        int maxX = Math.max(r1.getX1(), Math.max(r1.getX2(), Math.max(r2.getX1(), r2.getX2())));
        int minY = Math.min(r1.getY1(), Math.min(r1.getY2(), Math.min(r2.getY1(), r2.getY2())));
        int maxY = Math.max(r1.getY1(), Math.max(r1.getY2(), Math.max(r2.getY1(), r2.getY2())));

        return new Rectangle(minX, maxY, maxX, minY);
    }

    public static Rectangle intersection(Rectangle  r1, Rectangle r2){
        //TODO: how to check for overlapping areas. meanwhile always no overlap
        if(true){
            return new Rectangle();
        }else{
            return new Rectangle();
        }
    }

    public String toString(){
        String output = "Point1: (" + x1 + "/" + y1 + ") Point2: (" + x2 + "/" + y2 + ")";
        return output;
    }

    public boolean equals(Object obj){
        if(obj instanceof Rectangle){
            Rectangle rect = (Rectangle) obj;
            if(rect.getX1() == this.x1 && rect.getY1() == this.y1 && rect.getX2() == this.x2 && rect.getY2() == this.y2) {
                return true;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }
}

