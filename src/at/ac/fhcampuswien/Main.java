package at.ac.fhcampuswien;

import at.ac.fhcampuswien.shapes.Rectangle;

import java.sql.SQLOutput;

public class Main {

    public static void main (String[] args) {

        Rectangle r1 = new Rectangle(100,100);
        Rectangle r2 = new Rectangle(-120,-120,-20,-20);

        test(r1);
        test(r2);

        System.out.println(Rectangle.union(r1, r2));
    }

    public static void test(Rectangle r){
        System.out.println("WIDTH: " + r.getWidth());
        System.out.println("HEIGHT: " + r.getHeight());
        System.out.println("---");
        System.out.println("IS 1/1 INSIDE: " + r.isInside(1,1));
        System.out.println("MOVING IT BY 100/100");
        r.move(100,100);
        System.out.println("IS 1/1 NOW INSIDE: " + r.isInside(1,1));
        System.out.println("THERE ARE NOW " + r.getCount() + "RECTANGLES");
        System.out.println("---NEXT RECT---");
    }

}
